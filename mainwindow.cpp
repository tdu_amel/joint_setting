﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog.h>
#include <QString.h>
#include <QMessageBox.h>
#include <vtkRenderWindow.h>
#include <vtkAutoInit.h>
#include <boost/shared_ptr.hpp>
VTK_MODULE_INIT(vtkRenderingOpenGL)
//#pragma execution_character_set("utf-8")

#include "vsartificialheart.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    initGUI();
    initVTK();


}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::initVTK() {

    _qvtkWidget = new QVTKWidget;
    ui->gridLayout->addWidget(_qvtkWidget);
    ui->gridLayout->update();
    m_renderer = vtkRenderer::New();
    _qvtkWidget->GetRenderWindow()->AddRenderer(m_renderer);
    m_renderer->SetBackground(0,0,0);

}


void MainWindow::addCompoundSTL(QTreeWidgetItem* parent,boost::shared_ptr<vsArtificialHeart>& ah) {
/*
    QString stlFname(ah->name);
    vsTreeItem* item = new vsTreeItem(parent,QStringList(QString::fromAscii(QFileInfo(stlFname).fileName().toAscii())));


    if(parent) {

       // parent->addChild(item);
        //item->m_bHasRel = true;

    }else {
        //ui->treeWidget->insertTopLevelItem(0,item);
    }

    qDebug( "%s\n", qPrintable( stlFname ) );


   // MyCounter load("CompoundSTLTime.txt");
    //load.begin();

    vtkSmartPointer<vtkSTLReader> reader =
      vtkSmartPointer<vtkSTLReader>::New();
    reader->SetFileName(stlFname.toLocal8Bit());
    reader->Update();



    // Visualize
    vtkSmartPointer<vtkPolyDataMapper> mapper =
      vtkSmartPointer<vtkPolyDataMapper>::New();
    mapper->SetInputConnection(reader->GetOutputPort());

    actor_STL = vtkSmartPointer<vtkActor>::New();
    actor_STL->SetMapper(mapper);
    qDebug( "AddActor\n");
    m_renderer[0]->AddActor(actor_STL);



   // load.end();


    item->m_prop = actor_STL;
    m_treeItems.push_back(item);

    if( ah->firstChild ) {

        addCompoundSTL(item,ah->firstChild);

    }

    if( ah->nextSibling ) {

        addCompoundSTL(parent,ah->nextSibling);

    }

    m_renderer[0]->ResetCamera();
    this->ui->qvtkWidget1->GetRenderWindow()->Render();
*/

}


void MainWindow::loadCompoundSTL() {

    QString stlFname = QFileDialog::getOpenFileName(
            this,QString::fromLocal8Bit("複合STLの情報を記録したXMLファイルを開く") ,
            ".",
            QString::fromLocal8Bit( "XML file (*.xml)" ) );


    if ( !stlFname.isEmpty() )
    {

        boost::shared_ptr<vsArtificialHeart> ah(new vsArtificialHeart);

        qDebug( "compound:%s\n", qPrintable( stlFname ) );
        vtkSmartPointer<vsArtificialHeartParser> parser = vtkSmartPointer<vsArtificialHeartParser>::New();
        parser->parseXML(stlFname,ah);


        qDebug("parseXML:ok\n");
        printf("b:%s\n",qPrintable(ah->firstChild->name));
        addCompoundSTL((QTreeWidgetItem*)NULL,ah->firstChild);


    }

}

void MainWindow::initGUI() {

        ui->setupUi(this);

        //メニューの初期化
        initMenu();
}

void MainWindow::slotSaveJointInfo() {
    QMessageBox msg(this);
    msg.setText(QString::fromLocal8Bit("未実装です"));
    msg.exec();
}

void MainWindow::initMenu() {

    openCompoundSTLAct = new QAction(QString::fromLocal8Bit("&OpenSTL(Compound)"),this);
    openCompoundSTLAct->setStatusTip(QString::fromLocal8Bit("あらかじめパーツ分けされた複数のSTLをグループ化して読み込みます"));
    connect(openCompoundSTLAct, SIGNAL(activated()), this, SLOT(loadCompoundSTL()));


    saveJointInfo = new QAction(QString::fromLocal8Bit("&Save"),this);
    saveJointInfo->setStatusTip(QString::fromLocal8Bit("固定端情報をXMLファイルに書き込みます(未実装)"));
    connect(saveJointInfo, SIGNAL(activated()), this, SLOT(slotSaveJointInfo()));


    fileMenu = this->menuBar()->addMenu(QString::fromLocal8Bit(("ファイル(&F)")));
    fileMenu->addAction(openCompoundSTLAct);
    fileMenu->addAction(saveJointInfo);
}
