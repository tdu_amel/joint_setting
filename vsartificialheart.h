﻿#ifndef VSXMLREADER_H
#define VSXMLREADER_H

#include <boost/shared_ptr.hpp>

#include <list>
#include <vtkObject.h>
#include <vtkSmartPointer.h>
#include <string>
#include "tinyxml2.h"
#include <QString>
#include <QFileInfo.h>
#include <QDir.h>
class vtkActor;


struct vsArtificialHeart
{
    boost::shared_ptr<vsArtificialHeart> firstChild;
    boost::shared_ptr<vsArtificialHeart> nextSibling;
    QString name;
    std::string attr;

    vsArtificialHeart() {

    }
};




class vsArtificialHeartParser:public vtkObject
{
private:
    QString folderName;
public:
    static vsArtificialHeartParser *New();

    void parseXML(QString& file,boost::shared_ptr<vsArtificialHeart>& ah){

        folderName = QString(QFileInfo(file).dir().path() );

        std::string cStr = file.toLocal8Bit();
        size_t len = cStr.length()+1;
        char* chr = new char[len];
        memcpy(chr, cStr.c_str(), len);

        qDebug("fname:%s",chr);

        tinyxml2::XMLDocument xml;

        xml.LoadFile(chr);
        delete[] chr;
qDebug("xml:ok\n");
        tinyxml2::XMLElement* element = xml.FirstChildElement("artificialheart")->FirstChildElement("componemts");
qDebug("element:ok\n");
        getElement(ah,element);
qDebug("getElement:ok\n");
    }



    void getElement(boost::shared_ptr<vsArtificialHeart>& me,tinyxml2::XMLElement* element) {


        if( element == NULL )
        {
            return;
        }


            tinyxml2::XMLElement* child = element->FirstChildElement();
            if( child ) {
                qDebug("child");

                me->firstChild.reset(new vsArtificialHeart);
                getElement(me->firstChild,child);

            }




            if ( element->Attribute( "type","stl" ) ) {
                   //STL
                    const char* attr = element->Attribute( "name" );
                    qDebug("attr:%s",attr);
                    me->attr = attr;
                    me->name = folderName + QString::fromLocal8Bit("/") + QString::fromAscii(attr);
                    qDebug("Uattr:%s",qPrintable(me->name));
            }


           tinyxml2::XMLElement* elem_sibling = element->NextSiblingElement();
            if( elem_sibling ) {
                qDebug("sibling");

                me->nextSibling.reset(new vsArtificialHeart);
                getElement(me->nextSibling,elem_sibling);

            }

    }




};



//std::list< vtkSmartPointer<vsArtificialHeart> > obj;
typedef std::list< vtkSmartPointer<vsArtificialHeart> > vsArtificialHeartList;






#endif // VSXMLREADER_H
