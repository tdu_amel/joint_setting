﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)

#include <QMainWindow>
#include <vtkSmartVolumeMapper.h>
#include <vtkSmartPointer.h>
#include <vtkCollection.h>
#include <QMenu.h>
#include <QMenuBar.h>
#include <QVTKWidget.h>
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include <boost/shared_ptr.hpp>

class QTreeWidgetItem;
class vsArtificialHeart;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QVTKWidget* _qvtkWidget;
    QAction *openDicomAct;
    QAction *openSTLAct;
    QAction *openCompoundSTLAct;
    QAction *saveJointInfo;
    QMenu *fileMenu;
    QMenu *editMenu;

private:
    vtkSmartPointer<vtkRenderer> m_renderer;
public slots:
    void loadCompoundSTL();
    void slotSaveJointInfo();
private:
    void initVTK();
    void initGUI();
    void initMenu();

    void addCompoundSTL(QTreeWidgetItem* parent,boost::shared_ptr<vsArtificialHeart>& ah);
};

#endif // MAINWINDOW_H
